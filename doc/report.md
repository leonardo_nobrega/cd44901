h3. Squares data on Solr

This is a walkthrough of the r6 code that creates the squares report data shown by the UI, in preparation for the work to implement this feature on r7.

h3. Levski

!levski.png!

Levski adds topology info to the trackers during the generation of performance exceptions. It writes to the analytics_report Solr collection. It doesn't update the Cassandra tracker table.

When Levski starts, it uses the [CDI|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/prod/init.clj?fileviewer=file-view-default#init.clj-366] (prod.init) and [SolrCollectionWrapper|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/reports/solr_trackers.clj?fileviewer=file-view-default#solr_trackers.clj-48] (solr-trackers) components to retrieve topology from Solr collections populated by Parker. The components call the [reset-topology-caches!|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/reports/solr_trackers.clj?fileviewer=file-view-default#solr_trackers.clj-98] function. Levski stores the data in atoms inside the solr-trackers namespace.

There are five atoms: ctv, demarc, sep, analytics-test and build-uid. Each atom has a corresponding function that populates it. The names of these functions begin with cache-.

Reset-topology-caches! calls the cache-* functions according to the value passed as the types parameter. All cache-* functions call [query-topology!|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/reports/solr_trackers.clj?fileviewer=file-view-default#solr_trackers.clj-130].

The sep and ctv atoms are always empty in r6.
- The function [cache-sep-topology!|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/reports/solr_trackers.clj?fileviewer=file-view-default#solr_trackers.clj-199] which writes to the sep atom is not called.
- Neither [CDI|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/prod/init.clj?fileviewer=file-view-default#init.clj-385] nor [SolrCollectionWrapper|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/reports/solr_trackers.clj?fileviewer=file-view-default#solr_trackers.clj-65] use the :ctv type in their calls to reset-topology-caches!. So it never calls [cache-ctv-topology!|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/reports/solr_trackers.clj?fileviewer=file-view-default#solr_trackers.clj-182].

The [build-uid atom|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/reports/solr_trackers.clj?fileviewer=file-view-default#solr_trackers.clj-46] is not read by any of the solr-trackers functions.

Solr document fields query-topology! selects per entity-type:

|| entity type || fields ||
| [ctv|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/reports/solr_trackers.clj?at=r%2F6.x&fileviewer=file-view-default#solr_trackers.clj-134] | alias, id, network_element, oem, path, region, service_provider, site, sitecontact. switch |
| [demarc and sep|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/reports/solr_trackers.clj?at=r%2F6.x&fileviewer=file-view-default#solr_trackers.clj-148] | area, customer, layer-service, network-element, port, region, site, test-id, vendor |

The only field selected for ctv, demarc and sep is site (network element has an underscore for ctv and a dash for demarc and sep).

Levski attaches a [watcher|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/prod/init.clj?fileviewer=file-view-default#init.clj-370] to the Athena database. The watcher responds to the Athena swap by reloading the topology data from Solr. This is how Levski obtains the up-to-date Parker data. The CDI component manages the watcher.

[Performance-exception-calculation-execute|https://bitbucket.org/cenx-cf/levski/src/r/6.x/src/cenx/levski/performance_exception.clj?fileviewer=file-view-default#performance_exception.clj-185], [traffic-exception-job|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/traffic_exception.clj?at=r%2F6.x&fileviewer=file-view-default#traffic_exception.clj-38] and [traffic-exception-calculation-mbps|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/traffic_exception.clj?at=r%2F6.x&fileviewer=file-view-default#traffic_exception.clj-72] call update-trackers!. The data they pass to update-trackers! comes from Spark.

[Update-trackers!|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/reports/solr_trackers.clj?fileviewer=file-view-default#solr_trackers.clj-86] is the function in solr-trackers that receives the trackers data from the performance and traffic exception calculations.

[Augment-trackers|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/src/cenx/levski/reports/solr_trackers.clj?fileviewer=file-view-default#solr_trackers.clj-223] adds the data in the ctv, demarc and sep atoms to the trackers. Insert-trackers! writes the augmented trackers to the analytics_report Solr collection.

h3. Parker

!parker.png!

Parker writes topology information on Solr. The [Solr|https://bitbucket.org/cenx-cf/parker/src/bdfd906f102aa64ff3e11f5ce134d35a52277ca3/src/cenx/parker/build/solr/core.clj?at=integration&fileviewer=file-view-default#core.clj-49] component writes on the parker collection (ctv and sep). The [AnalyticsIndex|https://bitbucket.org/cenx-cf/parker/src/bdfd906f102aa64ff3e11f5ce134d35a52277ca3/src/cenx/parker/build/analytics_index/core.clj?at=integration&fileviewer=file-view-default#core.clj-31] component writes on the analytics_index collection (demarc).

h3. Bifrost

!bifrost.png!

The code that reads from analytics_report is in cenx.bifrost.analytics.core. A [case statement|https://bitbucket.org/cenx-cf/bifrost/src/f70cae3f49f49559e2311b1bbcbc4967d7576fcb/src/cenx/bifrost/analytics/core.clj?at=integration&fileviewer=file-view-default#core.clj-261] in the search function selects an entity type according to the report-type in the request. The entity types match the ones the augment-trackers function assigns to the Solr documents it creates: CosTestVector, Demarc, SegmentEndPoint.

h3. Size of Solr documents in QA-AGW

The attachment full-solr-doc comes from the [QA-AGW|https://cenx-cf.atlassian.net/wiki/spaces/KB/pages/178014535/Deployment+-+QA-AGW] system.

This is the [query|http://system01.qa-agw.cenx.localnet:8983/solr/analytics_report/select?q=*%3A*&rows=1&wt=json&indent=true] that retrieves the doc:

{code}
http://system01.qa-agw.cenx.localnet:8983/solr/analytics_report/select
?q=*:*
&rows=1
&wt=json
&indent=true
{code}

Most of the doc is names of sub-organizations and layer services (1066) in the fields layer-service_tiss, layer-service_ss, customer_tiss and customer_ss.

The attachment small-solr-doc is the same document without the layer services and sub-org fields. [Query|http://system01.qa-agw.cenx.localnet:8983/solr/analytics_report/select?q=*%3A*&rows=1&fl=total%2Cminor-trigger-period_l%2Ctime_over_minor%2Ctime_over_major%2Cvendor_tis%2Cend_date%2Cminor_threshold%2Caverage%2Cfrom_date%2Carea%2Carea_str%2Ctest-id_s%2Cfrom_timestamp%2Ctest-id_tis%2Cid%2Ccount%2Ctracker_key%2Csite_tis%2Cend_timestamp%2Cvendor_s%2Cunit_tis%2Cregion%2Cregion_str%2Cport%2Cmajor-trigger-period_l%2Cunit_s%2Clast_modified_timestamp%2Cnetwork-element_s%2Csite_s%2Cseverity%2Centity%2Cmetric_type%2Cmajor_threshold%2Cnetwork-element_tis%2C_version_&wt=json&indent=true]:

{code}
http://system01.qa-agw.cenx.localnet:8983/solr/analytics_report/select
?q=*:*
&rows=1
&wt=json
&indent=true
&fl=total,minor-trigger-period_l,time_over_minor,time_over_major,
vendor_tis,end_date,minor_threshold,average,
from_date,area,area_str,test-id_s,
from_timestamp,test-id_tis,id,count,
tracker_key,site_tis,end_timestamp,vendor_s,
unit_tis,region,region_str,port,
major-trigger-period_l,unit_s,last_modified_timestamp,network-element_s,
site_s,severity,entity,metric_type,
major_threshold,network-element_tis,_version_
{code}

The full doc size is 212 KB. The doc without the sub-orgs and layer-services is 1.9 KB.

!analytics-report-coll.png!

On 2017-11-09, the size of the analytics_report collection is 100 MB. There are 3078 documents. Each of these documents has a unique id and one of two metric types: mbps-in and mbps-out. The average size of a document is 32 KB.

h3. References

[CD-44901 jira ticket|https://cenx-cf.atlassian.net/browse/CD-44901]
[Exception calculations document|https://bitbucket.org/cenx-cf/levski/src/a057d0039a7dd7bcb6750d9f91772486c05a372a/doc/4_exception_calculations.md?fileviewer=file-view-default] section 4.4
[Solr schema for analytics_report|https://bitbucket.org/cenx-cf/docker-solr/src/integration/conf/analytics_report/conf/schema.xml?at=integration&fileviewer=file-view-default]
